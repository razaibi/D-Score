D-Score for User Experience Design
======
```Author: Raza Balbale```


```
Prologue

This is an attempt to grade and quantify a user experience design. 
Delivering a good user experience may not be merely piecing together flashy components. 
It is a lot about building sustainable practices and keeping in mind key considerations of good design. 
This evaluates key questions which are weighed differently, 
appreciating good design and penalizing key faults like badly addressed security. 

What is the D-Score?

The D-Score or Design Score is value on a scale of 10 based on different aspects
of UX design.


```

> **Usability**

1. What is the level of education/handholding required to use elements in the environment?

```
What is the net cost of training developers and end users?
The lesser user actions/ interactions need to be described, the more effective it is.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| -0.1	    	| 	0.1	 | 	0.2      |

###### _* The lower, the better._

2. What is the level on anticipation (of the next action) shown in the design? 

```
Areas in the user experience which leave the user wanting for more explanation on the 
next step/ action need to identified.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

3. What is the level of general goodness of fit (in terms of user experience) for the target demographic?

```
An end product/service is best evaluated by the users or consumers 
for whom it is designed for in the first place.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.2	    	| 	0.1	 | 	0.0      |

###### _* The higher, the better._

4. What is the level of smoothness at which services/features degrade?

```
As noted in application development, graceful degradation of the experience is important. 
Errors or interruptions should not stall usage completely and should be 
clearly communicated.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.1	 | 	-0.3     |

###### _* The higher, the better._

> **Aesthetic Appeal**

5. What is the level of visual appeal rated by the consumer of the solution?

```
The looks do matter! Use of context appropriate fonts, component sizes, etc. are important.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

6. What is the level of longevity of the design (will the user find the experience operable even 6 months later)?

```
It is important that experience are not designed just for today's technology and target audience.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.1	 | 	-0.2     |

###### _* The higher, the better._

7. What is the level of balance between technological compatibility and aesthetic appeal?

```
Compromising compatiblity and usability for aesthetic appeal may not be the best choice after all.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.2	    	| 	0.1	 | 	0.0      |

###### _* The higher, the better._

> **Compatibility**

8. In the world of a gaziliion devices, what is the level of compatibility across the target platforms?

```
It is important to cater to the different platforms on which the experience is delivered 
(without complete re-writes/ re-designs).
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

9. What is the level of customization (in design and code base) required to add compatiblity?

```
Time, effort and overall volume of changes need to be accounted for.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.7	    	| 	0.3	 | 	0.1      |

###### _* The higher, the better._

> **Performance**

10. To what level is every element, page and screen streamlined(minified, bundled, gzipped, etc.) to enhance performance?

```
Optimization is the key. Most fluid and memorable user experiences are not clunky and slow.
*This may be heavily dependant on the underlying architecture.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

11. What is the level of UX performance compared to other industry players?

```
It is good to compare against other players in the same space.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

12. What is the level of uniformity of performance across the target devices?

```
Consistency of experience is a key driving factor.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.5	    	| 	0.2	 | 	-0.2     |

###### _* The higher, the better._

> **Data Availability**

13. What is the number of interactions required to retrieve relevant data?

```
Making users search data pertinent to them just reflects bad design.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| -0.2	    	| 	0.2	 | 	0.3      |

###### _* The higher, the better._

14. What is the number of locations the user needs to peruse to interpret data?

```
Making sense of it all should not mean "connect the dots for hours". 
This may be very relevant in projects like data visualization.
Being quick, slick and accurate is of high value. 
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| -0.3	    	| 	0.1	 | 	0.3      |

###### _* The lower, the better._

> **Data Security**

15. What is level of the appropriate data visibility (right end user seeing the right data)?

```
This is a deal breaker when developing or building experiences in the business realm.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.7	    	| 	0.3	 | 	-0.7     |

###### _* The higher, the better._

16. What is the number of stray elements that hint availability of actions more than one is allowed?

```
Often overlooked, certain user experience elements provide fodder for data hacks.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.1	    	| 	0.2	 | 	0.3      |

###### _* The lower, the better._

17. What is the level of encryption/ blurring of hidden data like IDs to avoid any injection attempts?

```
Securing the design perimeter is key and needs to evaluated.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.2	    	| 	0.1	 | 	-0.1     |

###### _* The higher, the better._

> **Universality (Globalization)**

18. What is the level of provisioning in the user experience for all the target audience cultures (left to right, right to left, special needs)?

```
Truely global user experiences make truly global products.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

> **Navigability**

19. What is the level of complexity of navigation?

```
Bad or complex navigation usually stalls usability.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| -0.3	    	| 	0.1	 | 	0.3      |

###### _* The lower, the better._


20. What is the level of visibilty of navigation across screens?

```
Ability to navigate smoothly at all times is key to a good user experience.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.1	 | 	-0.3     |

###### _* The higher, the better._

21. What is the level of ease to access key navigational elements like profile, logout, login?

```
Certain functionalities may be termed as "essentials of user experience".
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.1	 | 	-0.3     |

###### _* The higher, the better._

> **Re-Usability**

22. What is the level of hard wiring or re-wiring that would be required to introduce new components?

```
From a development perspective, the key to good design is to minimize hard wiring.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| -0.3	    	| 	0.1	 | 	0.3      |

###### _* The lower, the better._

23. What is the level of re-usability of the design practice across platforms?

```
Using technologies and more importantly design practices 
across platforms may help in building a tightly knit product.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

> **Uniformity**

24. What is the level of two(or more) experience points looking like they have been designed by the same person?

```
The more the experience feels like it is driven by one thought process, the better it is.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

25. What is the level of two(or more) experience points looking like they have been designed for the same person?

```
Experiences across the product should not feel like they have been designed
for an edge case.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

26. What is the level of consistency between underlying data and design elements?

```
Often a challenge for most modern design technologies and architectures is to 
establish harmony between data and user experience elements.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.1	 | 	-0.3     |

###### _* The higher, the better._

> **Measurability**

27. What is the level of provisioning for capturing essential analytics' elements?

```
In order to understand nuances of user behavior and problem areas, it is essential 
to accomodate a good analytics' infrastructure. You cannot fix a problem that cannot
be identified and quantified. Provisioning for practices like A|B testing should be
addressed.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

> **Verbiage**

28. What is the level of complexity of terms and words in context of data presented the end user?

```
Clear conspicous language is the key.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| -0.1	    	| 	0.1	 | 	0.2      |

###### _* The lower, the better._

29. What is the level of complexity of imperative(actionable) messages in the user experience?

```
When considering actionable language, the importance of language
becomes all the more relevant.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| -0.1	    	| 	0.1	 | 	0.2      |

###### _* The lower, the better._

30. What is the level of standardization of metrics/textual elements in the ux item/ visualization?

```
Using standard metrics across the board, keep things credible.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.2	    	| 	0.1	 | 	-0.2     |

###### _* The higher, the better._

31. What is the level of balance in terms of text to visualization?

```
Do not overpopulate. One can find a book when one feels like reading.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._

> **Sustainablity**

32. What is the level of sustainability in terms of design principles?

```
Every project or solution may help identify key nuances and considerations 
to build a good user experience. The key is to identify them and be-able to
use the principles across domains.

Good design principles are sustainable and flexible.
```

| High		| Medium 	 | Low		 |
| :---          |     :---:      |          ---: |
| 0.3	    	| 	0.2	 | 	0.1      |

###### _* The higher, the better._


```
Epilogue

These questions may be graded differently to suit a scenario. 
The idea to help put together a flexible framework the helps put together a strong design experience.

The only way for this grading to be effective is through critical, honest and accurate evaluations.

```

> **Evaluation**

## <5.0 : Major Overhauls Needed

```
Pay attention to key details and try to address each aspect of experience design 
individually. Set achievable targets and take a phased approach.
```

## >5.0 & <7.5 : Improvement Needed

```
Identify key elements that weigh your score down and see how you can improve 
with minimal cost.
```

## >7.5 : Headed in the Right Direction

```
Ensure the good practices in your design process are sustained.
```
